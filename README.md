I thought I would make a C library for fun, this is my project I made with it

to do this, I used a Quora website to lead me to how you create a C library,
which I will like here:

https://www.quora.com/How-do-you-make-your-own-libraries-in-C-programming


you need the c file to hold the actual functions, and the h file to hold
all the prototypes for it to work.


in the program you need to run, (I included the testfile.c, but it can be
whatever file you want to use) you would have to add:
#include "jlibrary.h"


to be able to compile you would run:
gcc -c jlibrary.c -o jlibrary.o
gcc -c testfile.c -o testfile.o

to create the executable file you need:
gcc testfile.o jlibrary.o -o testfile

then to run:
./testfile

